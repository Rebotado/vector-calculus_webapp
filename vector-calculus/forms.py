from flask_wtf import FlaskForm
from wtforms import TextField, SelectField

COORDINATE_TYPE_CHOICES = (
    (0, 'Cilindricas'),
    (1, 'Rectangulares')
)

CALCULUS_OPERATION_CHOICES = (
    (0, 'Derivada'),
    (1, 'Integral')
)



class VectorCalculusForm(FlaskForm):
    operation_type = SelectField('Operacion', choices=CALCULUS_OPERATION_CHOICES)
    input = TextField('input')

class VectorCalculatorForm(FlaskForm):
    input = TextField('input')
    
class CoordinateConversorForm(FlaskForm):
    input = TextField('input')
    coordinate_type = SelectField('Tipo de coordenada', choices=COORDINATE_TYPE_CHOICES)

class PartialDerivativeForm(FlaskForm):
    input = TextField('input')
    operation_type = SelectField('Operacion', choices=CALCULUS_OPERATION_CHOICES)