from flask import Flask, render_template, url_for, request
from models import VectorCalculator, CoordinateConversor, VectorCalculus, PartialDerivative
from forms import VectorCalculatorForm, CoordinateConversorForm, VectorCalculusForm, PartialDerivativeForm
from models.VectorCalculus import CalculusOperation

app = Flask(__name__)
app.config.update(dict(
    SECRET_KEY='SECRETKEY',
    WTF_CSRF_SECRET_KEY='CSRFKEY'
))

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/calculadora-de-vectores', methods=['GET', 'POST'])
def vector_calculator():
    form = VectorCalculatorForm()
    result = None
    if request.method == 'POST':
        calculator = VectorCalculator()
        calculator.read_input(form.input.data)
        result = calculator.calculate()
    return render_template('apps/vector_calculator.html', form=form, result=result)

@app.route('/conversor-de-coordenadas', methods=['GET', 'POST'])
def coordinate_conversor():
    form = CoordinateConversorForm()
    result = None
    if request.method == 'POST':
        conversor = CoordinateConversor()
        conversor.read_input(form.input.data)
        result = conversor.convert(True if form.coordinate_type.data == '1' else False)
    return render_template('apps/coordinate_conversor.html', form=form, result=result)

@app.route('/calculo-de-vectores', methods=['GET', 'POST'])
def vector_calculus():
    form = VectorCalculusForm()
    result = None
    if request.method == 'POST':
        calculator = VectorCalculus()
        operation_type = CalculusOperation.DERIVATIVE if form.operation_type.data == '0' else CalculusOperation.INTEGRAL
        result = calculator.calculate(form.input.data, operation_type)
    return render_template('apps/vector_calculus.html', form=form, result=result)

@app.route('/integrales-y-derivadas', methods=['GET', 'POST'])
def integrals_and_derivative():
    form = PartialDerivativeForm()
    results = None
    if request.method == 'POST':
        calculator = PartialDerivative()
        operation_type = CalculusOperation.DERIVATIVE if form.operation_type.data == '0' else CalculusOperation.INTEGRAL
        calculator.calculate(form.input.data, operation_type)
        results = calculator.results
    return render_template('apps/calculus.html', form=form, results=results)




if __name__ == '__main__':
    app.run()

