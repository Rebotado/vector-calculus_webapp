import re
from sympy import *
from re import split, findall
from enum import IntEnum

class CalculusOperation(IntEnum):
    DERIVATIVE = 0
    INTEGRAL = 1



class InvalidInput(Exception):
    pass

class VectorCalculus:
    def calculate(self, input, calculus_operation):
        self._input = input
        translated_input = self._translate_input(input)
        if self._validate_input(translated_input):
            if calculus_operation == CalculusOperation.DERIVATIVE:
                return self._calculate_derivative(translated_input)
            elif calculus_operation == CalculusOperation.INTEGRAL:
                return self._calculate_integral(translated_input)
        else:
            raise InvalidInput('El formato no es valido.')

    def _translate_input(self, input):
        return list(filter(None, split('i|j|k', input.replace(' ', ''))))


    def _validate_input(self, input):
        unique_letters = ''.join(filter(str.isalpha,set(''.join(input))))
        if len(unique_letters) <= 1:
            return True
        else:
            return False

    def _calculate_derivative(self, input):
        vector_ids = self._get_vector_ids()
        result = ''
        t = Symbol('t')
        for x in range(len(input)):
            derivative = diff(input[x], t)
            if derivative == 0:
                continue
            result += f'{derivative}{vector_ids[x]}'
        return result.replace('**', '^').replace('*', '')

    def _get_vector_ids(self):
        return findall('i|j|k', self._input)

    def _calculate_integral(self, input):
        vector_ids = self._get_vector_ids()
        result = ''
        t = Symbol('t')
        for x in range(len(input)):
            integral = integrate(input[x], t)
            if integral == 0:
                continue
            result += f'({integral}{vector_ids[x]})'
            if x != len(input)-1:
                result += ' + '
        return result.replace('**', '^').replace('*', '')

