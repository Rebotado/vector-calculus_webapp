import re
import math

class EmptyInput(Exception):
    pass


class CoordinateConversor:

    def rectangularToCilindric(self):
        if self.coordinates:
            x = self.coordinates[0]
            y = self.coordinates[1]
            z = self.coordinates[2]
            coordinates = [
                math.sqrt(math.pow(x, 2) + math.pow(y, 2)), #r
                math.tan(x/y), #degree
                z #z
            ]
            return coordinates
        else:
            raise EmptyInput

    def cilindricToRectangular(self):
        if self.coordinates:
            r = self.coordinates[0]
            degree = self.coordinates[1]
            z = self.coordinates[2]
            coordinates = [
                r * math.cos(degree), #x
                r * math.sin(degree), #y
                z #z
            ]
            return coordinates
        else:
            raise EmptyInput

    def read_input(self, input):
        if self._validate_input(input):
            self.coordinates = self._string_to_number_array(input)


    def convert(self, isRectangular=True):
        if isRectangular:
            return self.rectangularToCilindric()
        else:
            return self.cilindricToRectangular()

    def _string_to_number_array(self, input):
        coordinate_regex = re.compile(r'(?<=\()(.*?)(?=\))')
        coordinate_input = coordinate_regex.search(input)
        if coordinate_input:
            result = coordinate_input.group(0).split(',')
        else:
            result = input.split(',')
        return [int(x) if float(x).is_integer else float(x) for x in result]

    def _validate_input(self, input):
        return True