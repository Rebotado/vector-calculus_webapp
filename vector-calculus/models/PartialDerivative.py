from .VectorCalculus import CalculusOperation
import re
from sympy import Symbol, integrate, diff

class PartialDerivative:
    
    def calculate(self, input, calculus_operation):
        self._input = input
        self._operation = calculus_operation
        self._translate_input()
        self._validate_input()
        self._calculate()


    def _translate_input(self):
        self._input = self._input.replace(' ', '')
        if self._operation == CalculusOperation.INTEGRAL:
            self._get_integral_variables()
        elif self._operation == CalculusOperation.DERIVATIVE:
            self._get_derivative_variables()
        self._get_variables()

        #Sympy can't read inputs like 3x, so those are replaced with 3*x
        new_input = ''
        for i in range(len(self._input)):
            if self._input[i] in self._variables:
                if i-1 >= 0:
                    if self._input[i-1].isdigit() or self._input[i-1].isalpha():
                        new_input += '*'
            new_input += self._input[i]
        self._input = new_input
        #More sympy translation
        self._input.replace('^', '**')

    def _calculate(self):
        if self._operation == CalculusOperation.INTEGRAL:
            self._calculate_integral()
        elif self._operation == CalculusOperation.DERIVATIVE:
            self._calculate_derivative()

    def _get_variables(self):
        regex = re.compile(r'(?![dD])[a-zA-Z]')
        result = regex.findall(self._input)
        self._variables = result


    def _get_integral_variables(self):
        regex = re.compile(r'[d](?![dD])[a-zA-Z]')
        result = ''.join(regex.findall(self._input))
        self._input = regex.sub('', self._input)
        regex = re.compile(r'(?![dD])[a-zA-Z]')
        self._derivative_variables = regex.findall(result)


    def _get_derivative_variables(self):
        regex = re.compile(r'[d][/][d](?![dD])[a-zA-Z]')
        result = ''.join(regex.findall(self._input))
        self._input = regex.sub('', self._input)
        regex = re.compile(r'(?![dD])[a-zA-Z]')
        self._derivative_variables = regex.findall(result)

    def _validate_input(self):
        pass

    def _calculate_derivative(self):
        self.results = dict()
        for variable in self._derivative_variables:
            symbol = Symbol(variable)
            self.results[str(symbol)] = self._format_result(diff(self._input, symbol))

    def _calculate_integral(self):
        self.results = dict()
        for variable in self._derivative_variables:
            symbol = Symbol(variable)
            self.results[str(symbol)] = self._format_result(integrate(self._input, symbol))


    def _format_result(self, result):
        result = str(result)
        result = result.replace('**', '^')
        new_input = ''
        for i in range(len(result)):
            if result[i] == '*':
                if i-1 >= 0:
                    if result[i] == '*':
                        try:
                            if ((result[i-1].isdigit() or result[i-1].isalpha()) and (result[i+1].isalpha())):
                                continue
                        except Exception:
                            pass
            new_input += result[i]

        result = new_input
        return result