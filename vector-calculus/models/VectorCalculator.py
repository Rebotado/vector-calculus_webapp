import re
from sympy.vector import CoordSys3D
from sympy.vector import VectorAdd
import numpy as np

class NotValidInput(Exception):
    pass


class VectorCalculator:
    def __init__(self, input=None):
        self.input = input.lower() if input else input

    def read_input(self, input):
        self.input = input.lower() if input else input

    def calculate(self):
        return self._calculate_input()

    #Shitty and simple way to convert, can do better
    def _string_to_vector(self, string):
        R = CoordSys3D('R')
        vector_regex = re.compile(r'(?<=\()(.*?)(?=\))')
        vector_multiplier_regex = re.compile(r'\d+(?=\()')
        vector_string = vector_regex.search(string).group(0)
        vector_multiplier_search = vector_multiplier_regex.search(string)
        vector_multiplier = vector_multiplier_search.group(0) if vector_multiplier_search else None 
        vector_multiplier = 1 if (vector_multiplier is None or vector_multiplier.strip() == '') else int(vector_multiplier) if float(vector_multiplier).is_integer else float(vector_multiplier)
        vector_array = [int(x)if float(x).is_integer else float(x)  for x in vector_string.split(' ')]
        vector = vector_multiplier * (vector_array[0] * R.i + vector_array[1] * R.j + vector_array[2] * R.k)
        return vector
    
    def _calculate_input(self):
        translated_input = self._translate_input()
        #Could be better if the input is validated before translation
        if self._is_valid_input(translated_input) is False:
            raise NotValidInput
        result = translated_input['vectors'][0]
        for x in range(len(translated_input['operations'])):
            result = self._execute_vector_operation(result, translated_input['vectors'][x+1], translated_input['operations'][x])
        if translated_input['extra_operations']['magnitude']:
            return result.magnitude()
        elif type(result) == VectorAdd:
            return self._format_vector_result(result)
        return result
        
    def _execute_vector_operation(self, vector1, vector2, operation):
        if operation == '+':
            return vector1 + vector2
        elif operation == '-':
            return vector1 - vector2
        elif operation == 'x':
            return vector1.cross(vector2)
        elif operation == '*':
            return vector1.dot(vector2)

    def _translate_input(self):
        operation_regex = re.compile(r'[+\-\*\/x](?!\d)')
        extra_operations = {}
        if('magnitude' in self.input) or (self.input.startswith('||') and self.input.endswith('||')):
            extra_operations['magnitude'] = True
        else:
            extra_operations['magnitude'] = False
        operations = operation_regex.findall(self.input)
        vectors = [self._string_to_vector(vector_string) for vector_string in operation_regex.split(self.input)]
        return {
            'operations': operations,
            'vectors': vectors,
            'extra_operations': extra_operations
        }

    #missing a lot fo validations
    def _is_valid_input(self, input):
        if '*' in input['operations'] and len(input['vectors']) > 2:
            return False
        else:
            return True

    #Shitty way of formatting results, could do it on a lower level using the model VectorAdd
    #perhaps, overriding __str__ or _repr__ form VectorAdd
    def _format_vector_result(self, result):
        return (str(result).replace('R.', '').replace('*', ''))