$(window).load(function(){
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                };
            };
        };
        return cookieValue;
    };

    $.fn.ajaxForm = function(){
        var endpoint = this.attr('endpoint');
        var $button = $(this.children(this.attr('button-class')));
        console.log($button)
        var form = $(this)

        $button.on('click', function(){
            url = form[0].id.value === '' ? endpoint : endpoint + form[0].id.value + '/';
            //Checks if there is a method attr on the button, if that is so, then uses that method for the ajax request, otherwise
            // uses the form method
            button_method = $(this).attr('method')
            type = (button_method !== undefined && button_method !== false) ? button_method : form.attr('method')
            $.ajax({
                type: type,
                url: url,
                data: form.serialize(),
                headers: {
                    'Accept-Language': 'es-es',
                    'X-CSRFToken': getCookie('csrftoken'),
                },
                success: function(data){
                    message = ''
                    $.toast({
                        heading: 'La operacion se realizo exitosamente',
                        text: message,
                        icon: 'success'
                    })
                    $(form).trigger('form:success', [data])
                },
                error: function(data){
                    var message = $.map(data.responseJSON, function(details, name){
                        return name + ': ' + details;
                    });
                    $.toast({
                        heading: 'Hubo un error',
                        text: message,
                        icon: 'error'
                    })
                    $(form).trigger('form:error')
                }
            });
        });
    }


})



