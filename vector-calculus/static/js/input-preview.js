$(window).load(function() { 
    $.fn.toggleVisible = function(value){
        if(value !== ''){
            $(this).show()
        } else {
            $(this).hide()
        }
    }
    $('[input-preview]').each(function() {
        var output = '[input-data=' + $(this).attr('output-id') + ']';
        if ($(output).is('a')) {
            $(output).attr('href', this.value);
            $(output).toggleVisible(this.value)
            $(this).keyup(function() {
                $(output).attr('href', this.value);
                $(output).toggleVisible(this.value)
                });
        } else if ($(output).is('img')) {
            $(output).attr('src', this.value);
            $(this).change(function() {
                $(output).attr('src', this.value);
            })
        } else {
            $(output).text(this.value);
            $(this).keyup(function() {
                $(output).text(this.value);
            }).change(function() {
                $(output).text(this.value);
            });
        }

    });

});
