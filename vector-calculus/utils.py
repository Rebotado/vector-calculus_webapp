import re
from sympy.vector import CoordSys3D

def string_to_vector(string):
    R = CoordSys3D('R')
    vector_regex = re.compile(r'(?<=\()\d \d \d(?=\))')
    string = vector_regex.search(string).group(0)
    vector_array = [int(x) for x in string.split(' ')]
    vector = vector_array[0] * R.i + vector_array[1] * R.j + vector_array[2] * R.k
    return vector
    
def read_vector_user_input(string):
    operation_regex = re.compile(r'[+\-\*\/]')
    operations = operation_regex.findall(string)
    vectors = [string_to_vector(vector_string) for vector_string in operation_regex.split(string)]
    return {
        'operations': operations,
        'vectors': vectors
    }